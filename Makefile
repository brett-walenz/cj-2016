######################################################################
# Jun Yang (junyang@cs.duke.edu)
#
# Usage:
#	make		Compile the whole thing and produce ${base}.pdf.
#			Xfig sources files in figs/ directory will be
#			automatically converted into .pdf and .pdf.tex files.
#	make clean	Remove temp and backup files automatically
#			created by latex, emacs, xfig, etc.  Will not
#			remove ${base}.pdf.

######################################################################
# Name of the main latex file (without .tex suffix); make will produce
# a PDF file with this name and .pdf suffix:
base = paper

# Source xfig files; make will automatically produce .pdf and .tex
# files for embedding these figures into latex:
xfigs = $(wildcard figs/*.fig)
xfigpdf = $(addsuffix .pdf,$(basename ${xfigs}))
xfigpdftex = $(addsuffix .pdf_t,$(basename ${xfigs}))

tikzpys = $(wildcard figs/*.tikz.py)
tikzs = $(basename ${tikzpys})

epsexprs = $(wildcard figs/expr/*.eps)
pdfexprs = $(addsuffix .pdf,$(basename ${epsexprs}))

######################################################################
# Commands:

PDFLATEX = pdflatex
# The following option to bibtex in effect forces inlining of the
# content of a crossref (rather than creating a separate bibitem for
# the crossref and referencing it):
BIBTEX = bibtex -min-crossrefs=100
FIG = xfig -spec
FIG2PDF = fig2dev -L pdftex -p portrait
FIG2PDFTEX = fig2dev -L pdftex_t -p
PYTHON = python
#EPS2PDF = ps2pdf -dEPSFitPage
EPS2PDF = ps2pdf -dEPSCrop

######################################################################
# Main rules:

.PHONY : ${base}.pdf clean

${base}.pdf : ${xfigpdf} ${xfigpdftex} ${tikzs} ${pdfexprs}
	${PDFLATEX} ${base}
	${BIBTEX} ${base}
	#cp paper-short.bbl paper.bbl
	${PDFLATEX} ${base}
	${PDFLATEX} ${base}

${xfigpdf} : %.pdf : %.fig
	${FIG2PDF} $< $@

${xfigpdftex} : %.pdf_t : %.pdf

${xfigpdftex} : %.pdf_t : %.fig
	${FIG2PDFTEX} $(addsuffix .pdf,$(basename $<)) $< $@

${tikzs} : %.tikz : %.tikz.py
	${PYTHON} $< > $@

${pdfexprs} : %.pdf : %.eps
	${EPS2PDF} $< $@

clean :
	rm -f *.bak *~ $(addsuffix .bak,${xfigs})
	rm -rf auto/
	rm -f ${xfigpdf} ${xfigpdftex}
	rm -f ${tikzs}
	rm -f ${pdfexprs}
	rm -f ${base}.log ${base}.aux ${base}.blg ${base}.bbl
